package com.example.android.miwok;

/**
 * Created by macintosh on 12/02/2018.
 */

public class Word {

    private String miwok_translation;
    private String english_translation;
    private final int  DEFAULT_IMAGE_ID = -1;
    // Drawable resource ID
    private int imageResourceID = DEFAULT_IMAGE_ID;
    private int audioResourceID;



    public Word(String miwok_translation, String english_translation,int audioResourceID) {
        this.miwok_translation = miwok_translation;
        this.english_translation = english_translation;
        this.audioResourceID = audioResourceID;
    }

    public Word(String miwok_translation, String english_translation, int mImageResourceId, int audioResourceID) {
        this.miwok_translation = miwok_translation;
        this.english_translation = english_translation;
        this.imageResourceID = mImageResourceId;
        this.audioResourceID = audioResourceID;

    }

    public String getMiwok_translation() {
        return miwok_translation;
    }

    public String getEnglish_translation() {
        return english_translation;
    }

    public int getImageResourceID() {
       return imageResourceID;
    }

    public boolean isHasImage(){
        return imageResourceID!=DEFAULT_IMAGE_ID;
    }

    public int getAudioResourceID() {
        return audioResourceID;
    }

    @Override
    public String toString() {
        String details= "";
        details+= getClass().getSimpleName();
        details+= "\nMiwok Trans: " + miwok_translation;
        details+="\nEnglish Trans: "+ english_translation;
        details+="\nImage Res: " + imageResourceID;
        details+="\nAudio Res: "+ audioResourceID;
        return details;
    }
}
