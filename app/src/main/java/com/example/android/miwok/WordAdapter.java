package com.example.android.miwok;

import android.content.Context;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macintosh on 12/02/2018.
 */

public class WordAdapter extends ArrayAdapter<Word> {

    private int backgroundColor;


    public WordAdapter(@NonNull Context context, ArrayList<Word> arrayList,int backgroundColor) {
        super(context, 0,arrayList);
        this.backgroundColor = backgroundColor;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;

        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }

        Word currentWord = getItem(position);

        // Find the TextView in the list_item.xml layout with the ID version_name
        TextView miwokTextView = (TextView) listItemView.findViewById(R.id.miwok_textview);
        TextView englishTextView = (TextView) listItemView.findViewById(R.id.english_textview);
        ImageView imageView = (ImageView) listItemView.findViewById(R.id.imageView);
        View textContainerView = listItemView.findViewById(R.id.LinearChildView);


        // Get the version name from the current AndroidFlavor object and
        // set this text on the name TextView
        miwokTextView.setText(currentWord.getMiwok_translation());
        englishTextView.setText(currentWord.getEnglish_translation());

        if(currentWord.isHasImage())
            imageView.setImageResource(currentWord.getImageResourceID());
        else{
            imageView.setVisibility(View.GONE);
        }


        int color = ContextCompat.getColor(getContext(),backgroundColor);
        textContainerView.setBackgroundColor(color);



        return listItemView;


    }
}
